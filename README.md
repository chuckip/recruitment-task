Instructions for use

1. Clone down the repository
2. cd into atomGym directory
3. run the following command: npm run serve

-------------------------------------------------------------------------

This project was created using Vue.js and npm packages: vue-stripe & vue-router. The skeleton of the project was created by running: "vue create atomGym" or on git bash: "winpty vue.cmd create atomGym".
To install the 2 npm packages run these commands:
- npm install @vue-stripe/vue-stripe
- npm install vue-router

The form consists of the following sections:
- Gym location
- Gym membership plan
- Gym membership plan price
- Name
- Surname
- Email
- Phone Number (UK)
- Address
- Postcode (UK)
- Age (+18)
- Terms and Conditions
- Newsletter subscription

The gym location, membership plans and prices are taken from the json file provided for this technical task. The json was edited to include the priceIds on the products created on Stripe's Dashboard.

Each input field has validation in the form of regex.

Once all the input fields and terms and conditions are selected/filled in you can submit the form. Once submitted you will be taken to a summary page of all your data. Than you can progress to a confirmation page telling the user they will have to pay. Clicking the pay now button will redirect the user to Stripe payment gateway. For testing purposes you can use the following card number: 4242424242424242 or alternatively any of these test numbers on this page: https://stripe.com/docs/testing#regulatory-cards. Once complete and successful you will be redirected to a success page or if your card fails you will be redirected to a failure page.
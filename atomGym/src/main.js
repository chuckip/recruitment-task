import Vue from 'vue';
import App from './App.vue';
import router from './router';
const gyms = require('./api.js'); 

Vue.config.productionTip = false;

new Vue({
    router,
    render: h => h(App, {
        props: {
            'gymData': gyms,
        },
    }),
}).$mount('#app');

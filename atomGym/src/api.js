const apiData = require('./api/gym.json');

const getGymData = (gymData) => {
    return Object.values(gymData);
}

module.exports = {
    gyms: getGymData(apiData),
}
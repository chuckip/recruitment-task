import Form from '@/components/Form.vue';
import Success from '@/components/Success.vue';
import Failure from '@/components/Failure.vue';

const routes = [
    {
        path: '/',
        name: 'form',
        title: 'Form',
        component: Form,
    },
    {
        path: '/success',
        name: 'success',
        title: 'Success',
        component: Success,
    },
    {
        path: '/failure',
        name: 'failure',
        title: 'Failure',
        component: Failure,
    },
];

export default routes;
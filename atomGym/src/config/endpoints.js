const baseEndpoint = process.env.VUE_APP_URL;

export default {
    baseEndpoint,
    success: `${baseEndpoint}/success`,
    failure: `${baseEndpoint}/failure`,
};
